module github.com/govwa

go 1.15

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/sessions v1.2.1
	github.com/julienschmidt/httprouter v1.3.0
)
